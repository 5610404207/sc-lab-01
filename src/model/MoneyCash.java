package model;

public class MoneyCash {
	private int total;
	private int cash;
	private int buy;

	
	public int gettotal(int total){
		return total;
	}
	public int setTotal(int total){
		return total;
	}
	
	public int addCash(int cash){
		total = total+cash;
		return total;
	}
	public int withdrawCash(int buy){ 
		total = total-buy;
		return total;
	}
	public String toString() {
		return "Your money in this card :" +" " + total;
	}

}
